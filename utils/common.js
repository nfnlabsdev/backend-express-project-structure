// Authentication
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
require('dotenv').config()

var hashPassword = function (password) {
    return new Promise(function (resolve, reject) {
      bcrypt.hash(password, process.env.SALT_ROUND, function (err, hash) {
        if (err) {
          err = false
          reject(err)
        } else {
          resolve(hash)
        }
      })
    })
  }

  var comparePassword = function (password, hash) {
    return new Promise(function (resolve) {
      bcrypt.compare(password, hash, function (err, result) {
        if (err) {
          err = false
          resolve(err)
        } else {
          resolve(result)
        }
      })
    })
  }

  var generateToken = function (data) {
    return new Promise(function (resolve) {
      jwt.sign(data, process.env.SECRET, (err, token) => {
        if (err) {
          resolve(err)
        } else {
          resolve(token)
        }
      })
    })
  }

  var getTokenVerify = function (token) {
    var data = {}
    return new Promise(function (resolve) {
      jwt.verify(token, process.env.SECRET, (err, payload) => {
        if (err) {
          data.error = true
          data.data = null
          resolve(data)
        } else {
          data.error = false
          data.data = payload
          resolve(data)
        }
      })
    })
  }

module.exports = { hashPassword, comparePassword, generateToken, getTokenVerify };
